cmake_minimum_required(VERSION 2.8)

PROJECT ( JIISTARTAPP )

  

INCLUDE_DIRECTORIES(  $ENV{JAVA_HOME}/include   )
INCLUDE_DIRECTORIES( $ENV{JAVA_HOME}/include/win32   )
 
 # Make sure the compiler can find include files from our Hello library. 
include_directories (${JIISTART_SOURCE_DIR}/jiistartlib) 

# Make sure the linker can find the Hello library once it is built. 
link_directories (${JIISTART_BINARY_DIR}/jiistartlib) 

# Add executable called "helloDemo" that is built from the source files 
# "demo.cxx" and "demo_b.cxx". The extensions are automatically found. 
add_executable (jiistart WIN32 main.cpp ) 

# Link the executable to the Hello library. 
target_link_libraries ( jiistart  JIISTARTLIB ) 