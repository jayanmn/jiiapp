

#include <windows.h>

#include "JvmStarter.h"
#include "guicon.h"


int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{

	RedirectIOToConsole();

	int argc;
	LPWSTR *commandLine= CommandLineToArgvW( GetCommandLineW(), &argc);

	JvmStarter &jvmstart =  JvmStarter();
	jvmstart.setArgs( commandLine, argc);
	
	jvmstart.CreateVm();
	jvmstart.InvokeClass("appmain/AppMain");


	jvmstart.start();
	return 0;


}