// JvmStarter.h: interface for the JvmStarter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_JVMSTARTER_H__45FAB851_9FE1_41FC_BB60_EA8532736A0D__INCLUDED_)
#define AFX_JVMSTARTER_H__45FAB851_9FE1_41FC_BB60_EA8532736A0D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <jni.h>

typedef __declspec(dllimport) jint (__stdcall * CreateJavaVMFunc)(
					JavaVM**,
					void**,
					void*);





class JvmStarter  
{
private :
	JavaVM *jvm;
	JavaVMInitArgs   sJavaVMInitArgs;
    JavaVMOption     psJavaVMOption[25];
	JavaVMAttachArgs attachArgs;
	jthrowable		jvmException;
	JNIEnv *env;
	wchar_t **unicodeArgs;
	int argCount;
	

	jint res;



    
public:

	JvmStarter();
	virtual ~JvmStarter();
	void start();
	void setArgs(wchar_t **wargs, int argc);

	JavaVM* CreateVm();

	void InvokeClass( char* mainClass);

	char* wchar2Utf8(wchar_t* utf16) ;

};

#endif // !defined(AFX_JVMSTARTER_H__45FAB851_9FE1_41FC_BB60_EA8532736A0D__INCLUDED_)
