// JvmStarter.cpp: implementation of the JvmStarter class.
//
//////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <windows.h>



using namespace std;



#include "JvmStarter.h"
using namespace std;




//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

JvmStarter::JvmStarter()
{

}

JvmStarter::~JvmStarter()
{

}


void JvmStarter::start() 
{
	for ( int i =0; i < 1000 ; i++ )
		cout <<  "hello, start" <<endl;
}


JavaVM* JvmStarter::CreateVm()
{
    
	JavaVMInitArgs vm_args;
	JavaVMOption options[3];

	options[0].optionString = "-Djava.class.path=C:/jnambood/scratch/AppMain/build/classes";
	options[1].optionString= "-verbose";
	options[2].optionString = "-verbose:jni";

	vm_args.version = JNI_VERSION_1_6;
	vm_args.options = options;
	vm_args.nOptions = 3;

	vm_args.ignoreUnrecognized = JNI_TRUE;

	 HMODULE hJVMLib = LoadLibraryW(L"C:\\apps\\32bit\\Java\\jdk1.6.0_25\\jre\\bin\\client\\jvm.dll");
	 if ( hJVMLib == NULL ) {
		MessageBoxW ( NULL, L"failed to load dll", L"error" , MB_OK);
		return NULL;
	 }
    CreateJavaVMFunc JNI_CreateJavaVM =
    (CreateJavaVMFunc) GetProcAddress(hJVMLib, "JNI_CreateJavaVM");
	if ( JNI_CreateJavaVM == NULL ) {
			MessageBoxW ( NULL, L"failed to locate  JNI_CreateJavaVM", L"error" , MB_OK);
			return NULL;
	}
    /* Create the Java VM */
	res = JNI_CreateJavaVM(&jvm, (void**)&env, &vm_args);
	if (JNI_CreateJavaVM == NULL) {
      MessageBoxW ( NULL, L"failed to create ", L"error" , MB_OK);
    }

	return jvm;

}
/*****************************************************************************
Function : Invoke the class
arguments:JNIEnv*
return :void
Comments :
*******************************************************************************/

void JvmStarter::InvokeClass(char* mainClass)
{

    jclass          jcJclass;
    jmethodID       jmMainMethod;
    

    int iarg_cntr = 0,icnt = 0;

	

    
    if(  jvm->GetEnv( (void**)&env, JNI_VERSION_1_6)  ){
        MessageBoxW(NULL, L"Could not get JNIEnv*",L"Net", MB_OK);
        return ;
    }

    //Check for JVM
    if(env == NULL){
		 MessageBoxW(NULL, L"Could not get JNIEnv* actual", L"Net", MB_OK);
        return;
	}

    //Find the class
    jcJclass = env->FindClass(mainClass);
	if(jcJclass == NULL)
    {
        MessageBoxW (NULL, L"Error: cannot find class.", L"ol" ,MB_OK);
        //return;
    }

    //Exception handling(If the class not found)
    jvmException = (env)->ExceptionOccurred();
    if(jvmException != NULL)
    {
		MessageBoxW (NULL, L"Exception: cannot find class.", L"" ,MB_OK);
         env->ExceptionDescribe();
         return ;
    }

    

    //Find the main method.
    jmMainMethod = env->GetStaticMethodID(jcJclass, "main", "([Ljava/lang/String;)V");
    if(jmMainMethod == NULL)
    {
        MessageBoxW (NULL, L"Error: cannot find methd id .", L"" ,MB_OK);
        return;
    }
	
	jclass stringClass = env->FindClass("java/lang/String");
	//error check


	jobjectArray javaApplicationArgs = env->NewObjectArray( argCount, stringClass, NULL);
	int iOption=0;

	//copy from exe args to java args
	//Copy the arguments to the jobjectArray
        for (int j=0; j < argCount; j ++){
           
            jstring tempArgString  = env->NewStringUTF(  wchar2Utf8( unicodeArgs[j])  );
            env->SetObjectArrayElement(javaApplicationArgs, iOption, tempArgString);
            jvmException = env->ExceptionOccurred();
            if(jvmException != NULL)
            {
                 env->ExceptionDescribe();
                 return ;
            }

			 iOption++;
        }



	
    

	//
	jvm->AttachCurrentThread( (void**)&env, &attachArgs);

    //Call the main method.
    env->CallStaticVoidMethod(jcJclass, jmMainMethod, javaApplicationArgs);
	

	jvmException = (env)->ExceptionOccurred();
    if(jvmException != NULL)
    {
		MessageBoxW (NULL, L"Exception: cannot find class.", L"" ,MB_OK);
         env->ExceptionDescribe();
         return ;
    }

	
	//jvm->DetachCurrentThread();


    //Destroy the JVM
    
    

}


void JvmStarter::setArgs(wchar_t **exeArgs, int argc) {
	unicodeArgs = exeArgs;
	argCount = argc;
}

char* JvmStarter::wchar2Utf8(wchar_t* utf16) {

		int utf8_length;



	utf8_length = WideCharToMultiByte(
	  CP_UTF8,           // Convert to UTF-8
	  0,                 // No special character conversions required 
						 // (UTF-16 and UTF-8 support the same characters)
	  utf16,             // UTF-16 string to convert
	  -1,                // utf16 is NULL terminated (if not, use length)
	  NULL,              // Determining correct output buffer size
	  0,                 // Determining correct output buffer size
	  NULL,              // Must be NULL for CP_UTF8
	  NULL);             // Must be NULL for CP_UTF8

	if (utf8_length == 0) {
	  // Error - call GetLastError for details
	}

	char* utf8 = (char*) malloc (sizeof(char) * 1000);

	utf8_length = WideCharToMultiByte(
	  CP_UTF8,           // Convert to UTF-8
	  0,                 // No special character conversions required 
						 // (UTF-16 and UTF-8 support the same characters)
	  utf16,             // UTF-16 string to convert
	  -1,                // utf16 is NULL terminated (if not, use length)
	  utf8,              // UTF-8 output buffer
	  utf8_length,       // UTF-8 output buffer size
	  NULL,              // Must be NULL for CP_UTF8
	  NULL);             // Must be NULL for CP_UTF8

	if (utf8_length == 0) {
	  // Error - call GetLastError for details
	}
	return utf8;

}


